Hooks.on('init', () => {

    if(typeof Babele !== 'undefined') {
        Babele.get().register({
            module: 'wfrp4e-warjama-es',
            lang: 'es',
            dir: 'compendium'
        });
    }
});
